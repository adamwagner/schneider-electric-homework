# About
Route distance Calculator is a homework project created as part of an application at Schneider Electric in June 2021.
The aim of the project is to create a script that processes a KML file describing the route of a vehicle and returns the total distance without taking incorrect data into account.

### The problem
Based on input to create the output.\
Input: Route of a vehicle stored in a KML file.\
Output: Total distance of the without incorrect measurement. 

### Calculate distance on a sphere
To calculate the distance between 2 coordinates I've used the [Haversine Formula](https://en.wikipedia.org/wiki/Haversine_formula).
As this approach assume the Globe's shape is like a perfect sphere the accuracy isn't the best to calculate long distances. However this can be accurate enough to measure the route of a vehicle within a city.

We expect that the coordinates submitted within the KML file are following the standard order - longitude, latitude, altitude - as it's documented on [Google's reference to KML files](https://developers.google.com/kml/documentation/kmlreference#linestring)

### Filter method
As the data has been recorded by a vehicle I expect that the frequency of data is predefined. This allows us to exclude incorrect data by comparing the actual distance of two points to some average metric. \
Analysing the input file it seems, every coordinate is recorded twice if the datapoint is correct (resulting in 0 distance between 2 following points). For further calculations I've excluded 0 distances from the list of all distances between points of the route.
There are 3 different methods to filter out incorrect measurements:
* Filtering method based on mean
  * If the distance between 2 points are greater than the mean of the route, it gets removed.
* Filtering method based o the route's deviation
  * If the difference of the mean of the route and the distance of 2 points greater than the deviation of the route
    the current distance gets removed.
* Filtering method based on neighbours deviation (default method)
  * If the difference of the mean of the close neighbours and the distance of 2 points greater than the
    deviation of the close neighbours the distance gets removed.
  * The number of close neighbours can be passed as argument (-n), which is 8 as default.
  
To change the filter method use argument -f or --filtering_method and use one of the accepted options:
* neighbours_deviation
* deviation
* mean

## Implementation
To resolve the issue I've used Python 3.9.5 and PyCharm.\
To run the script, you only need Python 3.9 The script doesn't use any 3rd party library only built in functions provided within Python 3.9.5 (argparse, xml.etree, math, statistics)

## Usage
The script can be called via command line interface with passing the KML file within a positional argument. \
There are 2 optional parameters can be used
* -f or --filtering_method
  * Three choices are allowed:
    * neighbours_deviation
    * deviation
    * mean
  * neighbours_deviation as default
* -n or --neighbours_to_consider
  * The number of neighbours that are taken into account on calculation
  * Default value is set to 8



### Further improvements opportunities
* Additional filtering methodology
  * Data points can be validated against roads
  * Knowing the actual data frequency speed can be calculated to exclude extremity
* Fine tuned distance calculation
  * i.e. Vincenty's formulae


