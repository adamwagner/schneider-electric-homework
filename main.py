import argparse
from xml.etree import ElementTree
from math import sin, cos, radians, asin, sqrt
from statistics import mean, pstdev


def evaluate_route(input_file, filter_method, number_of_neighbours):
    """Evaluates the route, require a single input file"""
    list_of_routes = proc_kml(input_file)

    for route in list_of_routes:
        all_distance = get_all_distance(route)
        filtered_distance = filter_track(all_distance, filter_method, number_of_neighbours)
        yield round(sum(filtered_distance), 2)


def proc_kml(kml_file):
    """Reads and process KML file. Returns all routes (list) stored as list of coordinate tuples."""
    tree = ElementTree.parse(kml_file)
    namespaces_dict = dict([node for (_, node) in ElementTree.iterparse(kml_file, events=['start-ns'])])

    routes = []

    for placemark in tree.findall('.//Placemark', namespaces=namespaces_dict):
        linestring = placemark.find('LineString', namespaces=namespaces_dict)
        coordinates = linestring.find('coordinates', namespaces=namespaces_dict).text
        routes.append(build_coordinates_tuple_list(coordinates))

    return routes


def build_coordinates_tuple_list(coordinate_string):
    """Split text to tuple elements, converts coordinates to float and returns them in a list."""
    list_of_coordinate_tuples = []

    route_coordinates = coordinate_string.split(' ')

    for entry in route_coordinates:
        if entry == '\n' or entry == '':
            continue
        # split into elements and transform to float
        try:
            list_of_coordinate_tuples.append(tuple(map(float, entry.split(','))))
        except ValueError:
            print("Invalid coordinates.")

    return list_of_coordinate_tuples


def position_distance(position_1, position_2):
    """Calculates the distance between 2 coordinates based on Haversine formula. Inputs as tuple of coordinates.
    Haversine formula: determines the great-circle distance between two points on a sphere given their longitudes
    and latitudes.
    https://en.wikipedia.org/wiki/Haversine_formula
    Expected order: longitude, latitude, altitude
    https://developers.google.com/kml/documentation/kmlreference#linestring"""
    long_1, lat_1 = map(radians, position_1[:2])
    long_2, lat_2 = map(radians, position_2[:2])

    earth_radius = 6371

    delta_lat = lat_2 - lat_1
    delta_lon = long_2 - long_1

    theta = 2 * asin(sqrt(sin(delta_lat / 2) ** 2 + cos(lat_1) * cos(lat_2) * sin(delta_lon / 2) ** 2))

    distance = theta * earth_radius

    return distance


def get_all_distance(full_path):
    """Calculate distance between points of path (list). Returns list of distances."""
    all_d = []
    for i in range(len(full_path)-1):
        all_d.append(position_distance(full_path[i], full_path[i + 1]))
    return all_d


def filter_track(list_of_distances, filter_type, number_of_neighbours):
    """Filters out 0 as no movement, and extreme changes (neighbours deviation, deviation, or mean)"""
    filtered_track = []

    for distance in list_of_distances:
        if not distance == 0:
            filtered_track.append(distance)

    if filter_type == "mean":
        filter_by_mean(filtered_track)
    elif filter_type == "deviation":
        filter_by_deviation(filtered_track)
    elif filter_type == "neighbours_deviation":
        filter_by_neighbours_deviation(filtered_track, number_of_neighbours)

    return filtered_track


def filter_by_mean(track):
    """Filtering method mean:
    If the distance between 2 points are greater than the mean of the route, it gets removed."""
    mean_of_route = mean(track)

    indexes_to_remove = []
    for i in range(len(track)):
        if track[i] > mean_of_route:
            indexes_to_remove.append(i)

    indexes_to_remove.reverse()
    for index in indexes_to_remove:
        del track[index]

    return track


def filter_by_deviation(track):
    """Filtering method deviation:
    If the difference of the mean of the route and the distance of 2 points greater than the deviation of the route
    it gets removed."""
    deviation_of_route = pstdev(track)
    mean_of_route = mean(track)

    indexes_to_remove = []
    for i in range(len(track)):
        if abs(track[i] - mean_of_route) > deviation_of_route:
            indexes_to_remove.append(i)

    indexes_to_remove.reverse()
    for index in indexes_to_remove:
        del track[index]

    return track


def filter_by_neighbours_deviation(track, number_of_neighbours):
    """Filtering method neighbours deviation:
    If the difference of the mean of the close neighbours and the distance of 2 points greater than the
    deviation of the close neighbours it gets removed."""
    # Add first and last element x(number_of_neighbours) times to the front/end
    updated_track = []
    updated_track += [track[0]] * number_of_neighbours + track + [track[-1]] * number_of_neighbours

    indexes_to_remove = []
    # Number_of_neighbours acts as start index
    for i in range(number_of_neighbours, number_of_neighbours + len(track)):
        sub_track_start_index = i - number_of_neighbours
        sub_track_end_index = i + number_of_neighbours
        sub_track = updated_track[sub_track_start_index:sub_track_end_index]

        mean_of_neighbours = mean(sub_track)
        dev_of_neighbours = pstdev(sub_track)

        if abs(updated_track[i] - mean_of_neighbours) > dev_of_neighbours:
            indexes_to_remove.append(i)
            # Note: Index to remove is updated_track's index

    indexes_to_remove.reverse()
    for index in indexes_to_remove:
        del track[index - number_of_neighbours]

    return track


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('kml_file',
                        help='KML input file with route coordinates')
    parser.add_argument('-f', '--filtering_method',
                        help='Filter method can be: "neighbours_deviation" (default), "deviation", "mean"',
                        choices=['neighbours_deviation', 'deviation', 'mean'],
                        default='neighbours_deviation')
    parser.add_argument('-n', '--neighbours_to_consider',
                        help='Neighbours to consider for neighbours_deviation filter method. 8 as default.',
                        default=8,
                        type=int)
    args = parser.parse_args()

    for dist in evaluate_route(args.kml_file, args.filtering_method, int(args.neighbours_to_consider)):
        print(f"{dist} km")
